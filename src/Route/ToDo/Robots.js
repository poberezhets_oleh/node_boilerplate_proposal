'use strict';

const express = require('express');
const path    = require('path');

module.exports = (app) => {
    const router = express.Router();

    router.get('/robots.txt', (req, res) => {
        res.sendFile(path.join(app.getConfig().publicPath, 'robots.txt'));
    });

    return router;
};
