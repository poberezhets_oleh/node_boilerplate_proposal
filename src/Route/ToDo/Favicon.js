'use strict';

const express = require('express');
const path    = require('path');

module.exports = (app) => {
    const router = express.Router();

    router.get('/favicon.ico', (req, res) => {
        res.sendFile(path.join(app.getConfig().publicPath, 'favicon.ico'));
    });

    return router;
};
