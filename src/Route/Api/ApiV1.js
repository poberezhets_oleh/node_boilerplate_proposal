'use strict';

const express = require('express');

const version = 'v1';

module.exports = (app) => {
    const router = express.Router();

    router.get(`/api/${version}/main`, (req, res) => {
        const response = {message: 'API main page'};

        res.send(response);
    });

    return router;
};
