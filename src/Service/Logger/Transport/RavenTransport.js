'use strict';

const clone = require('clone');
const winston = require('winston');

class RavenTransport extends winston.Transport {
    /**
     * @param {object} options
     * @param {object} options.raven
     */
    constructor(options) {
        options.name = 'raven';
        options.handleExceptions = options.handleExceptions || false;

        super(options);

        this._raven  = options.raven;
        this._levels = new Map([
            ['silly', 'debug'],
            ['verbose', 'debug'],
            ['info', 'info'],
            ['debug', 'debug'],
            ['warn', 'warning'],
            ['error', 'error']
        ]);
    }

    // noinspection JSUnusedLocalSymbols
    /**
     * @param {string} level
     * @param {string} message
     * @param {object} meta
     * @param {function} callback
     */
    log(level, message, meta = {}, callback) {
        if (meta instanceof Error) {
            const error = clone(meta);
            const ravenMeta = {
                level: this._levels.get(level)
            };

            if (error.extra) {
                ravenMeta.extra = error.extra;
                delete error.extra;
            }

            this._raven.captureException(error, ravenMeta, (err) => {
                if (err) {
                    process.nextTick(() => this.emit('error', err));

                    return callback ? callback(err) : undefined;
                }

                this.emit('logged');

                if (callback) {
                    callback(null, true);
                }
            });
        }
    }
}

module.exports = RavenTransport;
