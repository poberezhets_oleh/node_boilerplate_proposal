'use strict';

const _       = require('lodash');
const winston = require('winston');
const cycle   = require('cycle');

require('winston-daily-rotate-file');

const {
    timestampFormatter,
    messageFormatter
} = require('./src/Helper/formatter');

class FileTransport {
    /**
     * @returns {number}
     */
    static get MAX_DEEP_LEVEL_TO_CHECK_ERRORS() {
        return 5;
    }

    /**
     * @param {object} options
     * @param {string} options.filename
     */
    constructor(options = {}) {
        if (!options.filename) {
            throw new Error('File name is required for FileTransport');
        }

        this._filename         = options.filename;
        this._datePattern      = options.datePattern || 'yyyy-MM-dd.';
        this._prepend          = true;
        this._handleExceptions = options.handleExceptions || false;
        this._level            = options.level || 'info';
    }

    /**
     * @returns {winston.Transport}
     */
    create() {
        return new (winston.transports.DailyRotateFile)(
            {
                filename:         this._filename,
                datePattern:      this._datePattern,
                prepend:          this._prepend,
                handleExceptions: this._handleExceptions,
                timestamp:        timestampFormatter(),
                formatter:        messageFormatter(),
                level:            this._level,
                stringify:        FileTransport._stringify
            }
        );
    }

    /**
     * @param {Object} logObject
     * @returns {string}
     * @private
     */
    static _stringify(logObject) {
        const logObjectCopy = _.cloneDeep(logObject);

        const convertedLogObject = FileTransport._convertErrorFieldsToPlainObjects(logObjectCopy);

        return JSON.stringify(cycle.decycle(convertedLogObject));
    }

    /**
     * Changes each Error object within logObject to plain object with particular fields (name, message, stack, etc)
     * because `cycle.decycle` and `JSON.stringify` methods make Error objects empty ones.
     *
     * @param {Object} logObject
     * @param {number} [deep=0]
     * @param {Array} [errorPlainObjectsCache=[]] - already converted errors, reduce recursive method calls
     * @returns {Object} - returns the same logObject param
     * @private
     */
    static _convertErrorFieldsToPlainObjects(logObject, deep = 0, errorPlainObjectsCache = []) {
        if (deep >= FileTransport.MAX_DEEP_LEVEL_TO_CHECK_ERRORS) {
            return logObject;
        }

        Object.keys(logObject).forEach(key => {
            if (_.isError(logObject[key])) {
                const errorPlainObject = FileTransport._makePlainErrorObject(logObject[key]);

                logObject[key] = errorPlainObject;

                errorPlainObjectsCache.push(errorPlainObject);
            } else if (_.isObject(logObject[key]) && !errorPlainObjectsCache.includes(logObject[key])) {
                FileTransport._convertErrorFieldsToPlainObjects(logObject[key], deep + 1, errorPlainObjectsCache);
            }
        });

        return logObject;
    }

    /**
     * @param {Error} error
     * @returns {Object}
     * @private
     */
    static _makePlainErrorObject(error) {
        const errorPlainObject = {
            name:    error.constructor.name,
            message: error.message,
            stack:   error.stack
        };

        if (error.extra) {
            errorPlainObject.extra = FileTransport._convertErrorFieldsToPlainObjects(error.extra);
        }

        return errorPlainObject;
    }
}

module.exports = FileTransport;
