'use strict';

const winston = require('winston');

const RavenFactory = require('./RavenFactory');

const {
    RavenTransport,
    ConsoleTransport,
    FileTransport
} = require('./Logger/Transport');

class LoggerFactory {

    /**
     * @return {winston.Logger}
     */
    create(config) {
        const transports = [];

        Object.keys(config).forEach(transport => {
            const options = config[transport];

            if (transport === 'console') {
                transports.push(new ConsoleTransport(options).create());
            }

            if (transport === 'file') {
                transports.push(new FileTransport(options).create());
            }

            if (transport === 'raven') {
                const raven = new RavenFactory().create(options.sentryDsn);

                raven.install();

                const ravenTransportSettings = {
                    raven,
                    handleExceptions: options.handleExceptions
                };

                transports.push(new RavenTransport(ravenTransportSettings));
            }
        });

        // noinspection UnnecessaryLocalVariableJS
        const logger = new winston.Logger({
            transports:  transports,
            exitOnError: false,
            emitErrs:    true
        });

        return logger;
    }
}

module.exports = LoggerFactory;
