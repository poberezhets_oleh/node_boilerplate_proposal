'use strict';

const http = require('http');
const EventEmitter = require('events').EventEmitter;
const cuid = require('cuid');
const bodyParser = require('body-parser');
const express = require('restify');

const LoggerWithContextFactory = require('./Service/LoggerWithContextFactory');
const loggerWithContextFactory = new LoggerWithContextFactory();

const MAX_REQ_BODY_LENGTH = 2048;

class ApiApplication extends EventEmitter {

    static get STATUS_STOPPED() {
        return 1;
    }

    static get STATUS_STARTING() {
        return 2;
    }

    static get STATUS_STARTED() {
        return 3;
    }

    static get STATUS_STOPPING() {
        return 4;
    }

    /**
     * @param {{}} config
     * @param {RouteLoader} routeLoader
     * @param {winston} logger
     */
    constructor(config, routeLoader, logger) {
        super();
        this._config = config;
        this._routeLoader = routeLoader;
        this._logger = logger;

        this._id = cuid();
        /** @type {express|undefined} **/
        this._app = undefined;
        /** @type {Server|undefined} **/
        this._server = undefined;
        this._status = ApiApplication.STATUS_STOPPED;

        this._onServerError = this._onServerError.bind(this);
    }

    /** @return {string} */
    get CLASS_NAME() {
        return this.constructor.name;
    }

    /**
     * @return {{}}
     */
    getConfig() {
        return this._config;
    }

    /**
     * @return {string}
     */
    getId() {
        return this._id;
    }

    /**
     * @return {number}
     */
    getPort() {
        return this._config.port;
    }

    /**
     * @return {express}
     */
    getApp() {
        return this._app;
    }

    /**
     * @return {winston}
     */
    getLogger() {
        return this._logger;
    }

    /**
     * @return {ApiApplication}
     */
    async bootstrap() {
        const app = express();

        app.use(bodyParser.urlencoded({extended: true}));
        app.use(bodyParser.json());

        app.use((req, res, next) => {
            const loggerWithContext = loggerWithContextFactory.create(`@${cuid()}`, this._logger);
            let extra;

            if (req.method === 'POST') {
                const body = JSON.stringify(req.body);

                if (body.length > MAX_REQ_BODY_LENGTH) {
                    extra = body.substr(0, MAX_REQ_BODY_LENGTH) + '...(truncated)';
                } else {
                    extra = body;
                }
            }

            loggerWithContext.debug(`incoming from ${req.ip}: ${req.method} ${req.originalUrl}`, extra);

            req.logger = loggerWithContext;

            next();
        });

        await this._routeLoader.load(this, app, this._config.rootPath, this._config.routeDirPaths);

        app.use((req, res) => {
            if (!res.headersSent) {
                req.logger.info(`Unknown route path ${req.method} ${req.originalUrl}`);

                return res.status(500).json({error: {code: 1001, message: 'Unknown path'}});
            }
        });

        app.use((err, req, res, next) => {
            req.logger.error('Error handler middleware caught an error', err);

            return res.status(500).json({error: {code: 1000, message: 'Unknown error'}});
        });

        this._app = app;

        return this;
    }

    /**
     * @return {Promise}
     */
    start() {
        return new Promise((resolve, reject) => {
            if (this._status !== ApiApplication.STATUS_STOPPED) {
                return reject(new Error('Service is already started or is starting right now'));
            }

            const server = http.createServer(this._app);

            const onInitError = (error) => {
                this._status = ApiApplication.STATUS_STOPPED;

                this._logger.error(`${this.CLASS_NAME}::start() ApiApplication has failed to start`, error);

                reject(error);
            };

            const onListening = () => {
                server.removeListener('error', onInitError);
                server.on('error', this._onServerError);

                this._server = server;
                this._status = ApiApplication.STATUS_STARTED;

                resolve();
            };

            server.listen(this._config.port);
            server.once('error', onInitError);
            server.once('listening', onListening);

            this._status = ApiApplication.STATUS_STARTING;
        });
    }

    /**
     * @return {Promise}
     */
    stop() {
        return new Promise((resolve, reject) => {
            if (this._status !== ApiApplication.STATUS_STARTED) {
                reject(new Error('Service is not started'));
            }

            this._status = ApiApplication.STATUS_STOPPING;

            this._server.close(() => {
                this._status = ApiApplication.STATUS_STOPPED;
                this._server = undefined;
                resolve();
            });
        });
    }

    _onServerError(err) {
        this.emit('error', err);
    }
}

module.exports = ApiApplication;
