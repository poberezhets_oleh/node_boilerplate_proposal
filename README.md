# Node.js Project Boilerplate

## Installation

Execute ``yarn run build`` and ``yarn run start``.

See other useful ``yarn run ...`` commands in ``package.json``.

## Configuration

You can add to ``/config`` special files to override default environment config:

* ``local-development.json``
* ``local-production.json``
* ``local-test.json``

Files with ``local-*.json`` mask are ignored by git.

It's not recommended to use `.js` instead of `.json` because it may lead to misconfiguration and runtime errors.
Do it on your own risk!

## Details

This example contains two applications: TODO and API, each one listen own port.

TODO test commands:

* get todo records: ``curl -X GET http://localhost:1488/todo``
* add todo record: ``curl -X POST --data 'text=my todo' http://localhost:1488/todo``
* remove todo record: ``curl -X DELETE  --data 'text=my todo' http://localhost:1488/todo``

API test commands:

* get api main page: ``curl http://localhost:1489/api/v1/main``
